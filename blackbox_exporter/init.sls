# vim: tw=132
{% from "blackbox_exporter/map.jinja" import settings %}

blackbox_user:
  user.present:
    - name: {{ settings.system_username }}
    - shell: /sbin/nologin
    - groups:
      - ssl-cert

blackbox_binary:
  archive.extracted:
    - name: {{ settings.dist_dir }}
    - source: https://github.com/prometheus/blackbox_exporter/releases/download/v{{ settings.version }}/blackbox_exporter-{{ settings.version }}.linux-amd64.tar.gz
    - source_hash: {{ settings.archive_hash }}
    - keep: True

blackbox_create_symlink:
  file.symlink:
    - name: /usr/local/bin/blackbox-exporter
    - target: {{ settings.dist_dir }}/blackbox_exporter-{{ settings.version }}.linux-amd64/blackbox_exporter

blackbox_deploy_config:
  file.managed:
    - name: {{ settings.config_location }}
    - contents: |
        {{ settings.config|yaml(False)|string|indent(8) }}

blackbox_write_unitfile:
  file.managed:
    - name: /etc/systemd/system/blackbox-exporter.service
    - contents: |
        [Unit]
        After=network-online.target
        Requires=network-online.target

        [Service]
        User={{ settings.system_username }}
        Restart=on-failure
        ExecStart=/usr/local/bin/blackbox-exporter \
          --config.file={{ settings.config_location }}

        [Install]
        WantedBy=multi-user.target
blackbox_ensure_service:
  service.running:
    - name: blackbox-exporter.service
    - enable: True
    - watch:
      - file: blackbox_write_unitfile
      - file: blackbox_deploy_config
